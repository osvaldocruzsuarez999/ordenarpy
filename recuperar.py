import os
import shutil
import win32file
import win32api
import win32con

# Especificar el directorio de origen y el directorio de destino
origen = "F:/"
destino = "F:/recuperado"

# Recorrer los archivos y directorios en el disco
for raiz, directorios, archivos in os.walk(origen):
    for archivo in archivos:
        ruta = os.path.join(raiz, archivo)

        # Verificar si el archivo ha sido eliminado
        attrs = win32file.GetFileAttributes(ruta)
        if attrs & win32con.FILE_ATTRIBUTE_HIDDEN:
            # Copiar el archivo eliminado al directorio de destino
            ruta_destino = os.path.join(destino, archivo)
            shutil.copy2(ruta, ruta_destino)
            print(f"Archivo recuperado: {ruta}")
